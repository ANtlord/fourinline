#include "../header/GraphicManager.h"

GraphicManager::GraphicManager(){ }

GLuint GraphicManager::loadTexture(char * path) 
{
    GLuint texture;
    wxImage TexFile(wxString()<<path, wxBITMAP_TYPE_ANY, 100);
    if (TexFile.IsOk() == false)
        std::cout<<"File doesn't exists\n";

    else {
        glGenTextures(1,&texture);

        int width=TexFile.GetWidth();
        int height=TexFile.GetHeight();
        glBindTexture(GL_TEXTURE_2D, texture);

        GLubyte *bitmapData=TexFile.GetData();
        GLubyte *alphaData=TexFile.GetAlpha();

        const char RGB_CHANNELS = 3;
        const char RGBA_CHANNELS = 4;

        unsigned char bytesPerPixel = (alphaData != NULL) ?  RGBA_CHANNELS : RGB_CHANNELS;
        GLubyte *imageData=new GLubyte[width * height * bytesPerPixel];

        for(unsigned int y=0; y<height; y++) {        // To get every channel
            for(unsigned int x=0; x<width; x++) {     // by full width
                for (char i=0; i<bytesPerPixel; ++i){    // in every pixel column.
                    imageData[(x + y * width) * bytesPerPixel + i]=   // Set bitmap
                            bitmapData[( x + y * width) * 3 + i];   // to image.
                }
                if (alphaData != NULL)
                    imageData[(x + y * width) * bytesPerPixel + 3] = alphaData[x + y * width];
            }
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, bytesPerPixel, width, height, 0,
                     (alphaData != NULL ?  GL_RGBA : GL_RGB), GL_UNSIGNED_BYTE,
                     imageData);

        delete [ ] imageData;
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glShadeModel(GL_FLAT);

        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    return texture;
}

GLuint GraphicManager::texture = 0;

void GraphicManager::drawRectangle(GLfloat x1,GLfloat x2,
                                   GLfloat y1,GLfloat y2,GLfloat z)
{
    glBegin(GL_POLYGON);
        glVertex3f(x1,y1,z);
        glVertex3f(x1,y2,z);
        glVertex3f(x2,y2,z);
        glVertex3f(x2,y1,z);
    glEnd();
}

void GraphicManager::drawTexturedQuad(GLfloat quadCoords[5], GLfloat texCoords[4])
{
    glBegin(GL_QUADS);  //  Draws slot and sets texture on it.
        glTexCoord2f(texCoords[0], texCoords[2]);
        glVertex3f(quadCoords[0], quadCoords[2], quadCoords[4]);

        glTexCoord2f(texCoords[0], texCoords[3]);
        glVertex3f(quadCoords[0], quadCoords[3], quadCoords[4]);

        glTexCoord2f(texCoords[1], texCoords[3]);
        glVertex3f(quadCoords[1], quadCoords[3], quadCoords[4]);

        glTexCoord2f(texCoords[1], texCoords[2]);
        glVertex3f(quadCoords[1], quadCoords[2], quadCoords[4]);
    glEnd();
}
