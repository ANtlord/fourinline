/***************************************************************
 * Name:      FourInLineApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2013-03-25
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef FOURINLINEAPP_H
#define FOURINLINEAPP_H

#include <wx/app.h>

class FourInLineApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // FOURINLINEAPP_H
